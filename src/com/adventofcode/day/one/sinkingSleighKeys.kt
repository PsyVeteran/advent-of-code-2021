package com.adventofcode.day.one

import java.nio.file.Files
import java.nio.file.Paths

fun main(arguments: Array<String>) {
    val scans = inputReader();
    println("The scans showed the depth changed ${countDepthIncreases(scans)} times.")
}

/**
 * Counts the depth increases from the last reading.
 *
 * @param scans | A results as a String list.
 * returns -1 on scan results list empty
 * @return Integer tally of depth changes in the scans data.
 */
private fun countDepthIncreases(scans: List<String>): Int {
    var lastReading = 0
    var measurementIncreaseCounter = 0

    if (scans.isNotEmpty()) {
        lastReading = scans.first().toInt()
        for (line: String in scans) {
            if (line.isNotBlank()) {

                // Convert to Integer for compare.
                val currentDepthReading = line.toInt()

                if (currentDepthReading > lastReading) {
                    measurementIncreaseCounter += 1
                }
                lastReading = currentDepthReading
            }
        }
        return measurementIncreaseCounter
    }
    return -1
}


fun inputReader(): List<String> {
    return try {
        Files.readAllLines(Paths.get("src/com/adventofcode/day/one/scans"))
    } catch (exception: Exception) {
        listOf<String>()
    }
}